
organization in ThisBuild := "bi"

version := "0.0.1"

scalaVersion := "2.12.7"

def project(id: String) = Project(id, base = file(id))
  .settings(
    javacOptions in compile ++= Seq(
      "-encoding", "UTF-8",
      "-source", "1.8",
      "-target", "1.8",
      "-Xlint:unchecked",
      "-Xlint:deprecation"
    ),
    addCompilerPlugin("org.scalameta" % "paradise" % "3.0.0-M11" cross CrossVersion.full)
  )

lazy val utils = project("b-utils")
  .settings(
    version := "0.0.1",
    libraryDependencies ++= Seq(
      "cats-free"
    ).map("org.typelevel" %% _ % "1.6.1") ++ Seq(
      "circe-core",
      "circe-literal",
      "circe-generic",
      "circe-generic-extras",
      "circe-refined",
      "circe-parser"
    ).map("io.circe" %% _ % "0.11.1") ++ Seq(
      "junit-platform-commons"
    ).map("org.junit.platform" % _ % "1.5.0-M1") ++ Seq(
      "eu.timepit" %% "refined-cats" % "0.9.6",
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "org.projectlombok" % "lombok" % "1.16.18",
      "com.beachape" %% "enumeratum-circe" % "1.5.20",
      "org.scalactic" %% "scalactic" % "3.0.5",
      "org.scalatest" %% "scalatest" % "3.0.5",
      "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
      "com.lihaoyi" %% "sourcecode" % "0.1.7",
      "commons-httpclient" % "commons-httpclient" % "3.1",
      "com.alibaba" % "fastjson" % "1.1.35",
      "org.scala-lang" % "scala-reflect" % scalaVersion.value
    )
  )

val dependOpt = "test->test;compile->compile"

lazy val model = project(id = "b-model")
  .dependsOn(utils % dependOpt)
  .settings(
    version := "0.0.1"
  )

