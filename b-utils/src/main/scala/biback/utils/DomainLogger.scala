package biback.utils

import java.text.SimpleDateFormat
import java.util.Date

import ch.qos.logback.classic.{Level => XLevel}
import enumeratum._

object DomainLogger {

  sealed abstract class Level(val chLevel: XLevel) extends EnumEntry

  case object Level extends Enum[Level] with CirceEnum[Level] {

    case object DEBUG extends Level(XLevel.DEBUG)
    case object INFO extends Level(XLevel.INFO)
    case object ERROR extends Level(XLevel.ERROR)

    val values = findValues
  }

  case class LoggingLine(
                          file: String,
                          line: Int,
                          level: Level,
                          message: String,
                          timeStamp: Long = System.currentTimeMillis,
                          throwable: Option[Throwable] = None
                        ) extends DomainError {
    override def toString: String = {
      val shorted = file.substring(file.lastIndexOf('/') + 1)
      val simple = new SimpleDateFormat("HH:mm:ss.SSS")
      s"""${simple.format(new Date(timeStamp))} $level "$shorted:$line" - $message"""
    }
  }

  object Debug {
    def apply(message: => String)(implicit line: sourcecode.Line, file: sourcecode.File): LoggingLine =
      LoggingLine(file.value, line.value, Level.DEBUG, message)
  }

  object DebugArgs {
    def apply[V](value: sourcecode.Text[V])(implicit enclosing: sourcecode.Enclosing, line: sourcecode.Line, file: sourcecode.File): LoggingLine =
      LoggingLine(file.value, line.value, Level.DEBUG, s"${value.source} = ${value.value}")
  }

}
