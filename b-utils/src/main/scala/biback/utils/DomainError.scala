package biback.utils

trait DomainError extends Product with Serializable

final case class RefineError(rule: String) extends DomainError

final case class InvalidStateChange(from: String, to: String) extends DomainError

final case class InvalidCommand(command: Any) extends DomainError

final case class EntityEmpty(entityId: String) extends DomainError

final case class EntityExisted(entityId: String) extends DomainError

final case class MethodNoImplement(method: String) extends DomainError
