package biback.domain.batch

import biback.utils.GeneralAlgebra
import biback.domain.common.{BankAlgebra, BranchAlgebra}
import cats.Monad

class BatchService[F[_] : Monad](bank: BankAlgebra[F], branch: BranchAlgebra[F]) extends GeneralAlgebra[F] {

  def changeBranchStatus: ErrOrNoneF = ??? // change to Ready from SignOut
  def batchFee: ErrOrNoneF = ??? // 可以不放在日终
  def cutOff: ErrOrNoneF = ??? // 日切
  def joural2gl: ErrOrNoneF = ??? // 过账
  def changeBal: ErrOrNoneF = ??? // 改昨日余额活期定期内部户...

}