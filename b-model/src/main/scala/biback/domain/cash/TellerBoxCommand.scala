package biback.domain.cash

import biback.domain._
import biback.domain.common.TrnContext
import biback.domain.common.model.Currency
import biback.utils.ReplyType

sealed trait TellerBoxCommand extends DomainCommand

case class BoxRequire(ctx: TrnContext)
  extends TellerBoxCommand with ReplyType[Unit]

case class BoxCashDeposit(ctx: TrnContext, amount: BiDec, ccy: Currency)
  extends TellerBoxCommand with ReplyType[Unit]

case class BoxCashWithdraw(ctx: TrnContext, amount: BiDec, ccy: Currency)
  extends TellerBoxCommand with ReplyType[Unit]
