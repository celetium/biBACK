package biback.domain.cash

import biback.domain.InvalidEvent
import biback.utils._

class TellerBoxEntity extends DomainEntity {
  type Command = TellerBoxCommand
  type Event = TellerBoxEvent
  type State = TellerBox

  def commandHandler(s: TellerBox, c: TellerBoxCommand): CommandResult =
    c match {
      case i: BoxRequire => s.onCommand(i)
      case i: BoxCashDeposit => s.onCommand(i)
      case _ => Left(InvalidCommand(c))
    }

  def applyEvent(s: TellerBox, e: TellerBoxEvent): TellerBox =
    e match {
      case i: Initialized => TellerBox(i.tlrNo, BoxStatus.TurnIn)
      case i: BoxRequired => s.onEvent(i)
      case i: BoxBalanceChanged => s.onEvent(i)
      case _ => throw InvalidEvent(e)
    }

  def applyCreateEvent(e: TellerBoxEvent): TellerBox =
    this.applyEvent(null, e)
}