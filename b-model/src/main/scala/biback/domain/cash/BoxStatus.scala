package biback.domain.cash

import enumeratum._

sealed trait BoxStatus extends EnumEntry

case object BoxStatus extends Enum[BoxStatus] with CirceEnum[BoxStatus] {

  case object TurnIn extends BoxStatus
  case object Using extends BoxStatus
  case object Locked extends BoxStatus

  val values = findValues

}

