package biback.domain.cash

import biback.domain.common.TrnContext
import biback.utils.GeneralAlgebra
import cats.Monad

class CashService[F[_] : Monad] extends GeneralAlgebra[F] {
  def inStore(ctx: TrnContext): ErrOrNoneF = ???
  def outStore(ctx: TrnContext): ErrOrNoneF = ???
}
