package biback.domain.cash

import biback.domain._
import biback.domain.account.BalanceChangeType
import biback.domain.common.TrnContext
import biback.domain.common.model.Currency

sealed trait TellerBoxEvent extends DomainEvent

case class Initialized(tlrNo: TellerNo) extends TellerBoxEvent

case class BoxBalanceChanged(ctx: TrnContext, ccy: Currency, balancePre: BiDec, changeType: BalanceChangeType, amount: BiDec, balance: BiDec)
  extends TellerBoxEvent

case class BoxRequired()
  extends TellerBoxEvent
