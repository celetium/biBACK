package biback.domain

import biback.domain.common.model.Currency
import biback.utils.DomainError

package object cash {
  case object InvalidStatus extends DomainError

  case class CashBoxNotFound(ccy: Currency) extends DomainError

  case class CashBoxNotEnough(ccy: Currency) extends DomainError
}
