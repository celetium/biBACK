package biback.domain.cash

import biback.domain.PoDec
import biback.domain.common.model.Currency
import enumeratum.{CirceEnum, Enum, EnumEntry}
import eu.timepit.refined.auto._

sealed abstract class Cash(val currency: Currency, override val entryName: String, faceValue: PoDec) extends EnumEntry

case object Cash extends Enum[Cash] with CirceEnum[Cash] {
  import Currency._
  val values = findValues
  case object NOTE100YUAN extends Cash(CNY, "100元纸币", BigDecimal(100.00))
  case object NOTE050YUAN extends Cash(CNY,  "50元纸币", BigDecimal( 50.00))
  case object NOTE020YUAN extends Cash(CNY,  "20元纸币", BigDecimal( 20.00))
  case object NOTE010YUAN extends Cash(CNY,  "10元纸币", BigDecimal( 10.00))
  case object NOTE005YUAN extends Cash(CNY,   "5元纸币", BigDecimal(  5.00))
  case object NOTE001YUAN extends Cash(CNY,   "1元纸币", BigDecimal(  1.00))
  case object COIN001YUAN extends Cash(CNY,   "1元硬币", BigDecimal(  1.00))
  case object COIN005JIAO extends Cash(CNY,   "5角硬币", BigDecimal(  0.50))
  case object COIN001JIAO extends Cash(CNY,   "1角硬币", BigDecimal(  0.10))
}
