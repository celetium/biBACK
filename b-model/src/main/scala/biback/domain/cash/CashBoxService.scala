package biback.domain.cash

import biback.domain.common.TrnContext
import biback.utils.GeneralAlgebra
import cats.Monad

class CashBoxService[F[_] : Monad] extends GeneralAlgebra[F] {
  def create(ctx: TrnContext): ErrOrNoneF = ???
  def assign(ctx: TrnContext): ErrOrNoneF = ???
}
