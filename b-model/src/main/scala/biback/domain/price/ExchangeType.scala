package biback.domain.price

import enumeratum._

sealed trait ExchangeType extends EnumEntry

case object ExchangeType extends Enum[ExchangeType] with CirceEnum[ExchangeType] {

  case object Middle extends ExchangeType
  case object CashBuying extends ExchangeType
  case object CashSelling extends ExchangeType
  case object Buying extends ExchangeType
  case object Selling extends ExchangeType

  val values = findValues
}
