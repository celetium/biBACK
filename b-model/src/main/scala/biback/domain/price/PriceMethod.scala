package biback.domain.price

import enumeratum._

sealed trait PriceMethod extends EnumEntry

case object PriceMethod extends Enum[PriceMethod] with CirceEnum[PriceMethod] {

  case object Direct extends PriceMethod
  case object Indirect extends PriceMethod

  val values = findValues
}
