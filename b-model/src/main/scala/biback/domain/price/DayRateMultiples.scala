package biback.domain.price

import enumeratum._
import eu.timepit.refined.types.numeric.PosInt
import eu.timepit.refined.auto._

/**
  * 年利率转日利率的转换倍数。
  * @param x 倍数
  * @param termType 期限单位
  */
sealed abstract class DayRateMultiples(val x: PosInt, val termType: TermType) extends EnumEntry

case object DayRateMultiples extends Enum[DayRateMultiples] with CirceEnum[DayRateMultiples] {

  case object X360 extends DayRateMultiples(360, TermType.Year)
  case object X365 extends DayRateMultiples(365, TermType.Year)
  case object X366 extends DayRateMultiples(366, TermType.Year)
  case object X30  extends DayRateMultiples( 30, TermType.Month)
  case object X1   extends DayRateMultiples(  1, TermType.Day)

  val values = findValues
}
