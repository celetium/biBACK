package biback.domain.price

import enumeratum._

sealed trait IntCatalog extends EnumEntry

case object IntCatalog extends Enum[IntCatalog] with CirceEnum[IntCatalog] {

  case object Deposit extends IntCatalog
  case object Loan extends IntCatalog
  case object Interbank extends IntCatalog

  val values = findValues
}
