package biback.domain.price

import biback.domain._

case class BaseFee(
                  fee: FeeType,
                  price: NnDec,
                  method: FeeMethod,
                  effectiveDate: TrnDate
                  )

case class Fee(
              no: FeeNo,
              base: Fee,
              floatPercent: BiDec
              )
