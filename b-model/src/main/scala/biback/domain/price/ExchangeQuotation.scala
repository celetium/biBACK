package biback.domain.price

import biback.domain._
import biback.domain.common.model.Currency

case class ExchangeQuotation(
                              datetime: DateTime,
                              ccy1: Currency,
                              ccy2: Currency,
                              method: PriceMethod,
                              prices: Map[ExchangeType, PoDec]
                            ) {
}
