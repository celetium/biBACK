package biback.domain.price

import enumeratum._

sealed trait FeeMethod extends EnumEntry

case object FeeMethod extends Enum[FeeMethod] with CirceEnum[FeeMethod] {

  case object Fixed extends FeeMethod
  case object Rate extends FeeMethod
  case object Year extends FeeMethod
  case object Month extends FeeMethod

  val values = findValues
}
