package biback.domain.price

import enumeratum._

sealed trait FeeType extends EnumEntry

case object FeeType extends Enum[FeeType] with CirceEnum[FeeType] {

  case object AccountManagement extends FeeType
  case object UnionAgent extends FeeType
  case object AgentUnion extends FeeType

  val values = findValues
}
