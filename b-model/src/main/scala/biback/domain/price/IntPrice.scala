package biback.domain.price

import biback.domain._
import biback.domain.common.model.Currency

case class IntKey(
                   catalog: IntCatalog,
                   ccy: Currency,
                   termType: TermType,
                   term: NnInt
                 )

case class Floating(
                     upValue: NnInt,
                     downValue: NnInt,
                     upPercent: NnDec,
                     downPercent: NnDec
                   )

case class BaseInt(
                    key: IntKey,
                    rate: PoDec,
                    effectiveDate: TrnDate
                  )

case class IntPrice(
                     no: IntPriceNo,
                     intKey: IntKey,
                     floatPoint: Int,
                     floatPercent: BiDec,
                     isActualDays: Boolean,
                     multiples: DayRateMultiples,
                     adjust: AdjustMethod
                   ) {
  def dayRate(base: BaseInt): BiDec = {
    val usingRate = base.rate.value * (floatPercent / 100 + 1) + floatPoint / 10000
    usingRate / multiples.x.value / 100
  }
  def monthRate(base: BaseInt): BiDec = {
    val usingRate = base.rate.value * (floatPercent / 100 + 1) + floatPoint / 10000
    usingRate / 12 / 100
  }
}
