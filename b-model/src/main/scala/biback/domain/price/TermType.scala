package biback.domain.price

import enumeratum._

sealed trait TermType extends EnumEntry

case object TermType extends Enum[TermType] with CirceEnum[TermType] {

  case object Year extends TermType
  case object Month extends TermType
  case object Day extends TermType

  val values = findValues
}
