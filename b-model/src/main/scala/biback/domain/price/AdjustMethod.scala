package biback.domain.price

import enumeratum._

sealed trait AdjustMethod extends EnumEntry

case object AdjustMethod extends Enum[AdjustMethod] with CirceEnum[AdjustMethod] {

  case object No extends AdjustMethod
  case object According extends AdjustMethod
  case object Computation extends AdjustMethod

  val values = findValues
}
