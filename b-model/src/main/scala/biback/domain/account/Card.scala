package biback.domain.account

import biback.domain._
import biback.utils.DomainObject

case class Card
(
  id: PoNum,
  no: CardNo,
  kind: CardKind,
  isMain: Boolean = true,
  status: CardStatus = CardStatus.Normal
) extends DomainObject {

}
