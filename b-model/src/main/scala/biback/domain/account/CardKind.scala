package biback.domain.account

import enumeratum._

sealed trait CardKind extends EnumEntry

case object CardKind extends Enum[CardKind] with CirceEnum[CardKind] {

  case object CreditCard extends CardKind
  case object DepositCard extends CardKind

  val values = findValues
}