package biback.domain.account

import enumeratum._

sealed trait CertificateStatus extends EnumEntry

case object CertificateStatus extends Enum[CertificateStatus] with CirceEnum[CertificateStatus] {

  case object Blank extends CertificateStatus
  case object Taken extends CertificateStatus
  case object Nullified extends CertificateStatus

  val values = findValues
}
