package biback.domain

import biback.domain.common.model.Currency
import biback.utils.DomainError
import biback.domain.product.{BaseProduct, ProductCurrency}

package object account {
  case class ProductNotFound(product: BaseProduct, ccy: Currency) extends DomainError

  case class SubAcctNotFound(no: SubAccountNo) extends DomainError

  case class MultiSubAcctFound(product: ProductCurrency) extends DomainError

  case class SubAcctNotFoundOf(product: ProductCurrency) extends DomainError

  case class BalanceTypeNotFound(types: List[BalanceType]) extends DomainError
}
