package biback.domain.account

import enumeratum._

sealed trait BalanceType extends EnumEntry

case object BalanceType extends Enum[BalanceType] with CirceEnum[BalanceType] {

  case object AccountBalance extends BalanceType
  case object AvailableBalance extends BalanceType
  case object AccumulatedInterest extends BalanceType

  val values = findValues

}
