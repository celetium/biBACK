package biback.domain.account

import enumeratum._

sealed trait BalanceChangeType extends EnumEntry

case object BalanceChangeType extends Enum[BalanceChangeType] with CirceEnum[BalanceChangeType] {

  case object `+` extends BalanceChangeType
  case object `-` extends BalanceChangeType

  val values = findValues

}
