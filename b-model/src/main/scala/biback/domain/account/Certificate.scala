package biback.domain.account

import biback.domain._
import biback.utils.DomainObject

case class Certificate
(
  no: CertificateNo,
  cType: CertificateType,
  status: CertificateStatus = CertificateStatus.Blank
) extends DomainObject {

}

case class OneCertificate(cType: CertificateType, cNo: CertificateNo, ticketNo: Option[TicketNo] = None)