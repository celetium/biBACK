package biback.domain.account

import enumeratum._

sealed trait CertificateType extends EnumEntry

case object CertificateType extends Enum[CertificateType] with CirceEnum[CertificateType] {

  case object DepositCard extends CertificateType
  case object DepositBook extends CertificateType

  val values = findValues
}