package biback.domain.account

import enumeratum._

sealed trait AccountNature extends EnumEntry

case object AccountNature extends Enum[AccountNature] with CirceEnum[AccountNature] {

  case object PersonalSettle extends AccountNature
  case object PersonalNonSettle extends AccountNature

  val values = findValues
}