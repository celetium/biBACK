package biback.domain.account

import enumeratum._

sealed trait CardStatus extends EnumEntry

case object CardStatus extends Enum[CardStatus] with CirceEnum[CardStatus] {

  case object Normal extends CardStatus
  case object Eaten extends CardStatus
  case object Lost extends CardStatus
  case object Destroyed extends CardStatus

  val values = findValues
}