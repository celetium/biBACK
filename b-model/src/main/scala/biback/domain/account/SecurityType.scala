package biback.domain.account

import enumeratum._

sealed trait SecurityType extends EnumEntry

case object SecurityType extends Enum[SecurityType] with CirceEnum[SecurityType] {

  case object Password extends SecurityType
  case object Signature extends SecurityType

  val values = findValues
}