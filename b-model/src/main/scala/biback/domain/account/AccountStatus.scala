package biback.domain.account

import enumeratum._

sealed trait AccountStatus extends EnumEntry

case object AccountStatus extends Enum[AccountStatus] with CirceEnum[AccountStatus] {

  case object Normal extends AccountStatus
  case object Frozen extends AccountStatus
  case object Closed extends AccountStatus

  val values = findValues
}
