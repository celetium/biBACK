package biback.domain.common

import biback.domain.common.model.Branch
import biback.domain.{BranchNo, DomainCommand, DomainEvent, InvalidEvent, TellerNo}
import biback.utils._

class BranchEntity extends DomainEntity {
  type Command = BranchCommand
  type Event = BranchEvent
  type State = Branch

  def commandHandler(s: Branch, c: BranchCommand): CommandResult =
    c match {
      case _ => Left(InvalidCommand(c))
    }

  def applyEvent(s: Branch, e: BranchEvent): Branch =
    e match {
      case r: BranchAdded => r.branch
      case _ => throw InvalidEvent(e)
    }

  def applyCreateEvent(e: BranchEvent): Branch =
    this.applyEvent(null, e)
}

sealed trait BranchCommand extends DomainCommand

case class AddBranch(ctx: TrnContext, branch: Branch)
  extends BranchCommand with ReplyType[Unit]

case class BranchSignIn(ctx: TrnContext, brcNo: BranchNo)
  extends BranchCommand with ReplyType[Unit]

case class AddTeller(ctx: TrnContext, tlrNo: TellerNo)
  extends BranchCommand with ReplyType[Unit]

case class TellerSignIn(ctx: TrnContext, tlrNo: TellerNo)
  extends BranchCommand with ReplyType[Unit]

sealed trait BranchEvent extends DomainEvent

case class BranchAdded(branch: Branch)
  extends BranchEvent

case class TellerAdded(tlrNo: TellerNo)
  extends BranchEvent
