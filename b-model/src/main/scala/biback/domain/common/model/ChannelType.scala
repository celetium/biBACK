package biback.domain.common.model

import enumeratum._

sealed trait ChannelType extends EnumEntry

case object ChannelType extends Enum[ChannelType] with CirceEnum[ChannelType] {

  case object Web extends ChannelType
  case object Term extends ChannelType
  case object ATM extends ChannelType
  case object Union extends ChannelType

  val values = findValues
}