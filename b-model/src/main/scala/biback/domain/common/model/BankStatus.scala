package biback.domain.common.model

import enumeratum._

sealed trait BankStatus extends EnumEntry

case object BankStatus extends Enum[BankStatus] with CirceEnum[BankStatus] {

  case object Day extends BankStatus
  case object Night extends BankStatus

  val values = findValues
}