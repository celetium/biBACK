package biback.domain.common.model

import biback.domain._
import biback.domain.common.InvalidBankStatus
import biback.utils.{DomainObject, ReplyType}

case class Bank
(
  trnDay: TrnDate,
  status: BankStatus = BankStatus.Day
) extends DomainObject {
  def onCommand(c: NextTrnDay): CommandResult[c.ReplyType, TrnDayChanged] =
    for {
      _ <- status == BankStatus.Day ?= InvalidBankStatus(status)
      t <- trnDay.nextDay(1)
    } yield TrnDayChanged(t, trnDay)

  def onEvent(e: TrnDayChanged): Bank =
    copy(trnDay = e.trnDay)
}

sealed trait BankCommand

case class NextTrnDay(trnDay: Option[TrnDate] = None) extends BankCommand with ReplyType[Unit]

sealed trait BankEvent extends DomainEvent

case class TrnDayChanged(trnDay: TrnDate, preTrnDay: TrnDate) extends BankEvent
