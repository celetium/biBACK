package biback.domain.common.model

import enumeratum._

sealed trait BranchCatalog extends EnumEntry

case object BranchCatalog extends Enum[BranchCatalog] with CirceEnum[BranchCatalog] {

  case object Business extends BranchCatalog
  case object Management extends BranchCatalog
  case object Summary extends BranchCatalog

  val values = findValues
}
