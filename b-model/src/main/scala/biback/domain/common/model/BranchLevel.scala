package biback.domain.common.model

import biback.domain.NnInt
import biback.domain.common.IsLowestLevel
import biback.utils.DomainError
import enumeratum._
import eu.timepit.refined.auto._

sealed abstract class BranchLevel(val level: NnInt) extends EnumEntry

case object BranchLevel extends Enum[BranchLevel] with CirceEnum[BranchLevel] {

  case object Headquarter extends BranchLevel(0)
  case object Superiors extends BranchLevel(1)
  case object Branches extends BranchLevel(2)

  val values = findValues

  def nextLevel(oLevel: Option[BranchLevel]): Either[DomainError, BranchLevel] =
    oLevel match {
      case Some(level) =>
        level match {
          case Headquarter => Right(Superiors)
          case Superiors => Right(Branches)
          case Branches => Left(IsLowestLevel(level))
        }
      case None => Right(Headquarter)
    }
}
