package biback.domain.common.model

import enumeratum._

/**
  * 机构营业状态。
  */
sealed trait BranchBusinessStatus extends EnumEntry

case object BranchBusinessStatus extends Enum[BranchBusinessStatus] with CirceEnum[BranchBusinessStatus] {

  case object Ready extends BranchBusinessStatus
  case object SignIn extends BranchBusinessStatus
  case object Checking extends BranchBusinessStatus // 扎帐中
  case object SignOut extends BranchBusinessStatus

  val values = findValues
}
