package biback.domain.common.model

import biback.domain._
import biback.domain.common.{AddTeller, TellerAdded, TrnContext}
import biback.utils.DomainObject
import cats.data.Ior

case class Branch
(
  no: BranchNo,
//  name: Name,
//  address: Address,
//  phone: Phone,
  level: BranchLevel,
//  catalog: BranchCatalog,
//  deptNo: DeptNo,
  status: BranchBusinessStatus = BranchBusinessStatus.SignIn,
  tellers: List[Teller] = List.empty[Teller]
) extends DomainObject {
  def addTeller(ctx: TrnContext, tlrNo: TellerNo): CommandResult[Unit, Branch] = {
    val b = copy(tellers = tellers ++ List(Teller(tlrNo, no)))
    Right(Ior.right[Unit, Branch](b))
  }
}

case class Teller(no: TellerNo, branch: BranchNo)

case class Address(nation: Nation, province: Province, area: Area, city: City, district: District, street: Name)

case class Nation(code: NationCode, name: Name)
case class Province(code: ProvinceCode, name: Name)
case class District(code: DistrictCode, name: Name)
case class Area(code: AreaCode, name: Name)
case class City(code: CityCode, name: Name)
