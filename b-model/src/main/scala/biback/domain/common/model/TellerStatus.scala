package biback.domain.common.model

import enumeratum._

/**
 * 柜员状态。
 */
sealed trait TellerStatus extends EnumEntry

case object TellerStatus extends Enum[TellerStatus] with CirceEnum[TellerStatus] {

  case object Locked extends TellerStatus
  case object Ready extends TellerStatus
  case object SignIn extends TellerStatus
  case object Checking extends TellerStatus // 扎帐中
  case object SignOut extends TellerStatus

  val values = findValues
}

