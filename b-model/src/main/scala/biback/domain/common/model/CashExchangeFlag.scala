package biback.domain.common.model

import enumeratum._

sealed trait CashExchangeFlag extends EnumEntry

case object CashExchangeFlag extends Enum[CashExchangeFlag] with CirceEnum[CashExchangeFlag] {
  val values = findValues
  case object Neither extends CashExchangeFlag
  case object Cash extends CashExchangeFlag
  case object Exchange extends CashExchangeFlag
}
