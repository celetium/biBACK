package biback.domain.common.model

import biback.domain._
import enumeratum._
import eu.timepit.refined.auto._

/**
  * 币种。
  * @param minCalcIntScale 最小计息单位
  * @param minIntScale 利息精确位数
  */
sealed abstract class Currency(
                                val minCalcIntScale: NnInt,
                                val minIntScale: NnInt
                              ) extends EnumEntry

case object Currency extends Enum[Currency] with CirceEnum[Currency] {
  val values = findValues
  case object CNY extends Currency(NnInt0, 2)
  case object USD extends Currency(NnInt0, 2)
  case object HKD extends Currency(NnInt0, 2)
}