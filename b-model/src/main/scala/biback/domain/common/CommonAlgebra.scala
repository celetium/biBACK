package biback.domain.common

import biback.domain._
import biback.utils.{GeneralAlgebra, MethodNoImplement}
import cats.Monad

trait CommonAlgebra[F[_]] {
  val common: CommonAlgebra.Service[F]
}

object CommonAlgebra {

  trait Service[F[_]] extends GeneralAlgebra[F] {
    def smsValidated(mobile: Mobile, sms: SMSVCode)(implicit F: Monad[F]): ErrOrNoneF =
      orF(Left(MethodNoImplement("smsValidated")))
  }

  def noImpl[F[_]] = new Service[F] {}
}
