package biback.domain.common.service

import biback.domain._
import biback.domain.common.{BranchAlgebra, BranchEntity, TellerSignIn, TrnContext}
import biback.utils.{GeneralAlgebra, RegistryAlgebra}
import cats.Monad

class TellerSignService[F[_] : Monad]
(
  algebra: RegistryAlgebra[F] with BranchAlgebra[F]
) extends GeneralAlgebra[F] {
  import algebra._
  def signIn(ctx: TrnContext, rq: SignRequest): ErrOrNoneF = {
    for {
      t <- branch.foundTeller(rq.tlrNo)
      p <- registry.refFor[BranchEntity](t.branch.value)
      e <- p.ask(TellerSignIn(ctx, rq.tlrNo))
    } yield e
  }

  def signOut(ctx: TrnContext, rq: SignRequest): ErrOrNoneF = ???
  def checkout(ctx: TrnContext, rq: SignRequest): ErrOrNoneF = ???
  def changePassword(ctx: TrnContext, rq: SignRequest): ErrOrNoneF = ???
  def resetPassword(ctx: TrnContext, rq: SignRequest): ErrOrNoneF = ???
  def lock(ctx: TrnContext, rq: SignRequest): ErrOrNoneF = ???
  def unlock(ctx: TrnContext, rq: SignRequest): ErrOrNoneF = ???
}

case class SignRequest
(
  tlrNo: TellerNo
)
