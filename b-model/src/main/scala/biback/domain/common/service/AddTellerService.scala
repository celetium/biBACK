package biback.domain.common.service

import biback.domain._
import biback.domain.common.{AddTeller, BranchAlgebra, BranchEntity, TrnContext}
import biback.utils.{GeneralAlgebra, RegistryAlgebra}
import cats.Monad

class AddTellerService[F[_] : Monad]
(
  algebra: RegistryAlgebra[F] with BranchAlgebra[F]
) extends GeneralAlgebra[F] {
  import algebra._
  def add(ctx: TrnContext, rq: AddTellerRequest): ErrOrNoneF = {
    for {
      _ <- branch.found(rq.brcNo)
      n <- branch.createTlrNo()
      e <- branch.addTeller(rq.brcNo, ctx, n)
    } yield e
  }
  def drop(ctx: TrnContext, rq: SignRequest): ErrOrNoneF = ???
  def active(ctx: TrnContext, rq: SignRequest): ErrOrNoneF = ???
}

case class AddTellerRequest
(
  brcNo: BranchNo
)
