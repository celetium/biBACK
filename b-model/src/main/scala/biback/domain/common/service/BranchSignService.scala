package biback.domain.common.service

import biback.domain.common.{BranchAlgebra, BranchEntity, BranchSignIn, TrnContext}
import biback.utils.{GeneralAlgebra, RegistryAlgebra}
import cats.Monad

class BranchSignService[F[_] : Monad]
(
  algebra: RegistryAlgebra[F] with BranchAlgebra[F]
) extends GeneralAlgebra[F] {
  import algebra._
  def signIn(ctx: TrnContext, rq: AddUpBranchRequest): ErrOrNoneF = {
    for {
      t <- branch.found(rq.brcNo)
      p <- registry.refFor[BranchEntity](rq.brcNo.value)
      e <- p.ask(BranchSignIn(ctx, rq.brcNo))
    } yield e
  }

  def signOut(ctx: TrnContext, rq: AddUpBranchRequest): ErrOrNoneF = ???
  def checkout(ctx: TrnContext, rq: AddUpBranchRequest): ErrOrNoneF = ???
}