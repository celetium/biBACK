package biback.domain.common

import biback.domain._
import biback.utils.GeneralAlgebra

trait SecurityAlgebra[F[_]] extends GeneralAlgebra[F] {
  def passwordValidated(account: AccountNo, password: Cipher16): ErrOrNoneF
}
