package biback.domain.common

import biback.domain.InvalidEvent
import biback.domain.common.model.{Bank, BankCommand, BankEvent, NextTrnDay, TrnDayChanged}
import biback.utils._

class BankEntity extends DomainEntity {
  type Command = BankCommand
  type Event = BankEvent
  type State = Bank

  def commandHandler(s: Bank, c: BankCommand): CommandResult =
    c match {
      case i: NextTrnDay => s.onCommand(i)
      case _ => Left(InvalidCommand(c))
    }

  def applyEvent(s: Bank, e: BankEvent): Bank =
    e match {
      case i: TrnDayChanged => s.onEvent(i)
      case _ => throw InvalidEvent(e)
    }

  def applyCreateEvent(e: BankEvent): Bank =
    this.applyEvent(null, e)
}