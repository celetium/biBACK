package biback.domain

import biback.domain.common.model.{BankStatus, BranchLevel, ChannelType}
import biback.utils.DomainError

package object common {
  case class BankNotFound(no: BankNo) extends DomainError

  case class TellerBranchNoMatch(teller: TellerNo, branch: BranchNo) extends DomainError

  case class BranchNotFound(no: BranchNo) extends DomainError

  case class BranchFound(no: BranchNo) extends DomainError

  case class IsLowestLevel(level: BranchLevel) extends DomainError

  case class SomeBranchesNotFound(noSet: Set[BranchNo]) extends DomainError

  case class TellerNotFound(no: TellerNo) extends DomainError

  case class ChannelNoFound(channel: ChannelType) extends DomainError

  case class SMSInvalid(mobile: Mobile, sms: SMSVCode) extends DomainError

  case class InvalidBankStatus(status: BankStatus) extends DomainError

  case object HeadBranchNotFound extends DomainError

  case object HeadBranchFound extends DomainError
}
