package biback.domain.customer

import biback.domain._
import biback.domain.cash.{BoxCashDeposit, TellerBoxEntity}
import biback.domain.common.TrnContext
import biback.domain.product.BaseProduct.{CurrentDeposit1st, CurrentDeposit2nd}
import biback.domain.product.{BaseProduct, ProductAlgebra, ProductParam}
import biback.utils.RegistryAlgebra
import cats.Monad

class AccountDepositService[F[_] : Monad]
(
  algebra: RegistryAlgebra[F] with PersonalAlgebra[F] with ProductAlgebra[F]
) extends DomainService[F, CashDepositRequest, Unit] {

  import algebra._

  def run(ctx: TrnContext, rq: CashDepositRequest): ErrOrNoneF = {
    for {
      s <- customer.foundCustomerProduct(rq.acctId, rq.subAcctNo)
      a <- product.foundParam(s.product.base)
      _ <- orF(depositPreCheck(ctx, rq, s.product.base, a))
      t <- registry.refFor[TellerBoxEntity](ctx.oper.tlrNo.get.toString)
      _ <- t.ask(BoxCashDeposit(ctx, rq.trnAmt.value, s.product.ccy))
      p <- registry.refFor[PersonalEntity](s.cNo.value)
      e <- p.ask(CashDeposit(ctx, rq.acctId, rq.subAcctNo, rq.trnAmt.value, a)) // TODO: 支持TCC
    } yield e
  }

  // 部分支取
  def withdrawPartial(): ErrOrNoneF = ???

  // 取息
  def withdrawInterest(): ErrOrNoneF = ???

  // 续存
  def renewDeposit(): ErrOrNoneF = ???

  def depositPreCheck(ctx: TrnContext, o: CashDepositRequest, product: BaseProduct, param: ProductParam): ErrOrNone = {
    import ProductParam._
    val inputs = new TrnChannelSetter with InvokerSetter {
      val trnChannel = ctx.channel
      val invoker = ctx.oper.tlrNo
    }
    product match {
      case CurrentDeposit1st =>
        CurrentDeposit1st.depositPre(param, inputs)
      case CurrentDeposit2nd =>
        CurrentDeposit2nd.depositPre(param, inputs)
    }
  }
}

case class CashDepositRequest
(
  acctId: AccountId,
  subAcctNo: SubAccountNo,
  trnAmt: NnDec
)
