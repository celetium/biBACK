package biback.domain

import biback.utils.DomainError

package object customer {
  case class NoRegistration(mobile: Mobile) extends DomainError

  case class CustomerNotFound(acctId: AccountId) extends DomainError

  case class CustomerIdNotFound(idType: PersonalIdType, idNo: IdNo) extends DomainError

  case class MobileRegistered(mobile: Mobile) extends DomainError

  case object NonDepositProduct extends DomainError

  case class AccountNotFound(acctId: AccountId) extends DomainError
}
