package biback.domain.customer

import biback.domain._
import biback.domain.common.TrnContext
import biback.domain.product.{ProductParam, SaleableProduct}
import biback.utils.ReplyType

sealed trait PersonalCommand extends DomainCommand

case class IdValidate(ctx: TrnContext, cNo: PersonalCustomerNo, idType: PersonalIdType, idNo: IdNo)
  extends PersonalCommand with ReplyType[Unit]

case class OpenAccount(ctx: TrnContext, acctId: AccountId, acctNo: AccountNo, product: SaleableProduct, param: ProductParam)
  extends PersonalCommand with ReplyType[Unit]

case class CashDeposit(ctx: TrnContext, acctId: AccountId, subAcctNo: SubAccountNo, trnAmt: BiDec, param: ProductParam)
  extends PersonalCommand with ReplyType[Unit]

case class CashWithdraw(ctx: TrnContext, acctId: AccountId, subAcctNo: SubAccountNo, trnAmt: BiDec, param: ProductParam)
  extends PersonalCommand with ReplyType[Unit]

case class OpenSubAccount(ctx: TrnContext, acctId: AccountId, product: SaleableProduct, param: ProductParam)
  extends PersonalCommand with ReplyType[Unit]

case class CalcInterest(ctx: TrnContext, acctId: AccountId, subAcctNo: SubAccountNo, intAmt: BiDec)
  extends PersonalCommand with ReplyType[Unit]

case class Accumulate(ctx: TrnContext, acctId: AccountId)
  extends PersonalCommand with ReplyType[Unit]
