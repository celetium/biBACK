package biback.domain.customer

import enumeratum._

sealed trait CustomerCatalog extends EnumEntry

case object CustomerCatalog extends Enum[CustomerCatalog] with CirceEnum[CustomerCatalog] {

  case object Personal extends CustomerCatalog
  case object Company extends CustomerCatalog
  case object FinanceInstitute extends CustomerCatalog

  val values = findValues
}