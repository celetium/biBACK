package biback.domain.customer

import biback.domain.customer.CustomerStatus._
import biback.domain._
import biback.utils._

trait Customer extends DomainObject {
  val no: CustomerNo
  val status: CustomerStatus
}

case class Personal
(
  no: CustomerNo,
  mobile: Mobile,
  status: CustomerStatus,
) extends Customer with DomainObject {
  def onCommand(c: IdValidate): CommandResult[c.ReplyType, IdValidated] =
    for {
      _ <- status ?~> Normal
    } yield IdValidated(c.idType, c.idNo)

  def onEvent(e: IdValidated): Personal =
    copy(status = Normal)
}
