package biback.domain.customer

import biback.utils.StateChangeValidation
import enumeratum._

sealed trait CustomerStatus extends EnumEntry

case object CustomerStatus extends Enum[CustomerStatus] with CirceEnum[CustomerStatus] with StateChangeValidation[CustomerStatus] {

  case object Raw extends CustomerStatus
  case object Normal extends CustomerStatus
  case object Black extends CustomerStatus
  case object Gray extends CustomerStatus

  val values = findValues

  override def canChange(from: CustomerStatus, to: CustomerStatus): Validated = {
    (from, to) match {
      case (Raw, Normal) => valid
      case (_, _) => invalid(from, to)
    }
  }
}

