package biback.domain.customer

import biback.domain._
import biback.domain.common.{CommonAlgebra, TrnContext}
import biback.utils.RegistryAlgebra
import cats.Monad

class PersonalRegisterService[F[_] : Monad] (
  algebra: RegistryAlgebra[F] with PersonalAlgebra[F] with CommonAlgebra[F]
) extends DomainService[F, RegisterRequest, Unit] {

  import algebra._

  def run(ctx: TrnContext, r: RegisterRequest): ErrOrNoneF =
    for {
      _ <- customer.notRegistered(r.mobile)
      _ <- common.smsValidated(r.mobile, r.sms)
      n <- customer.createNo(CustomerCatalog.Personal)
      p <- registry.refFor[PersonalEntity](n.value)
      e <- p.create(Registered(n, r.mobile))
    } yield e
}

case class RegisterRequest
(
  mobile: Mobile,
  sms: SMSVCode
)

