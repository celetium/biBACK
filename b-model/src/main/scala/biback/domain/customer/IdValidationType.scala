package biback.domain.customer

import biback.utils.StateChangeValidation
import enumeratum._

sealed trait IdValidationType extends EnumEntry

case object IdValidationType extends Enum[IdValidationType] with CirceEnum[IdValidationType] with StateChangeValidation[IdValidationType] {

  case object NoValidated extends IdValidationType
  case object PoliceValidated extends IdValidationType
  case object FourFactorValidated extends IdValidationType
  case object FiveFactorValidated extends IdValidationType
  case object CounterValidated extends IdValidationType
  case object CounterFiveFactorValidated extends IdValidationType

  val values = findValues

  override def canChange(from: IdValidationType, to: IdValidationType): Validated = {
    (from, to) match {
      case (NoValidated, PoliceValidated) => valid
      case (NoValidated, FourFactorValidated) => valid
      case (PoliceValidated, FiveFactorValidated) => valid
      case (PoliceValidated, CounterValidated) => valid
      case (FiveFactorValidated, CounterFiveFactorValidated) => valid
      case (CounterValidated, CounterFiveFactorValidated) => valid
      case (_, _) => invalid(from, to)
    }
  }
}

