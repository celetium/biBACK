package biback.domain.customer

import biback.domain.InvalidEvent
import biback.utils._

class PersonalEntity extends DomainEntity {
  type Command = PersonalCommand
  type Event = PersonalEvent
  type State = PersonalCustomer

  def commandHandler(s: PersonalCustomer, c: PersonalCommand): CommandResult =
    c match {
      case i: IdValidate => s.onCommand(i)
      case i: OpenAccount => s.onCommand(i)
      case i: CashDeposit => s.onCommand(i)
      case i: CalcInterest => s.onCommand(i)
      case _ => Left(InvalidCommand(c))
    }

  def applyEvent(s: PersonalCustomer, e: PersonalEvent): PersonalCustomer =
    e match {
      case r: Registered => PersonalCustomer(Personal(r.cNo, r.mobile, CustomerStatus.Raw))
      case i: IdValidated => s.onEvent(i)
      case i: AccountOpened => s.onEvent(i)
      case i: BalanceChanged => s.onEvent(i)
      case _: NoEvent.type => s
      case _ => throw InvalidEvent(e)
    }

  def applyCreateEvent(e: PersonalEvent): PersonalCustomer =
    this.applyEvent(null, e)
}