package biback.domain.customer

import biback.domain._
import biback.domain.common.TrnContext
import biback.utils.RegistryAlgebra
import cats.Monad

class PersonalIdentifyService[F[_] : Monad]
(
  algebra: RegistryAlgebra[F]
) extends DomainService[F, IdValidateRequest, Unit] {

  import algebra._

  def run(ctx: TrnContext, rq: IdValidateRequest): ErrOrNoneF =
    for {
      p <- registry.refFor[PersonalEntity](rq.cNo.value)
      e <- p.ask(IdValidate(ctx, rq.cNo, rq.idType, rq.idNo))
    } yield e
}

case class IdValidateRequest(cNo: PersonalCustomerNo, idType: PersonalIdType, idNo: IdNo)