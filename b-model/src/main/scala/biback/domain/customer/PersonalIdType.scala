package biback.domain.customer

import enumeratum._

sealed trait PersonalIdType extends EnumEntry

case object PersonalIdType extends Enum[PersonalIdType] with CirceEnum[PersonalIdType] {

  case object IdCard extends PersonalIdType

  val values = findValues

}

