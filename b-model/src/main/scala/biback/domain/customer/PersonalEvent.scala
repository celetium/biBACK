package biback.domain.customer

import biback.domain._
import biback.domain.account.{BalanceChangeType, BalanceType}
import biback.domain.common.TrnContext
import biback.domain.product.SaleableProduct
import cats.data.NonEmptyMap

sealed trait PersonalEvent extends DomainEvent

case object NoEvent extends PersonalEvent

case class Registered(cNo: CustomerNo, mobile: Mobile)
  extends PersonalEvent

case class IdValidated(idType: PersonalIdType, idNo: IdNo)
  extends PersonalEvent

case class AccountOpened(ctx: TrnContext, acctId: AccountId, acctNo: AccountNo, subAcctNo: SubAccountNo, product: SaleableProduct, quotaBrcNo: BranchNo)
  extends PersonalEvent

case class SubAccountOpened(ctx: TrnContext, acctId: AccountId, subAcctNo: SubAccountNo, product: SaleableProduct, quotaBrcNo: BranchNo)
  extends PersonalEvent

case class BalanceChanged(ctx: TrnContext, acctId: AccountId, subAcctNo: SubAccountNo, balances: NonEmptyMap[BalanceType, BiDec], changeType: BalanceChangeType, amount: BiDec, fee: BiDec = BiDec0)
  extends PersonalEvent