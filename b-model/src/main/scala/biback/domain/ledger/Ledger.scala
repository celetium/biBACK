package biback.domain.ledger

import biback.domain._
import biback.domain.account._
import biback.domain.common._
import AccountingAmountType._
import biback.domain.common.model.Currency

final case class Ledger(
                         id: PoNum,
                         brcNo: BranchNo,
                         ccy: Currency,
                         subjectNo: SubjectNo,
                         serialNo: Int,
                         bal: BiDec,
                         status: AccountStatus
                       )

final case class ProductAccounting
(
  productName: Name,
  subjectNo: SubjectNo,
  direction: AccountingDirection
)

final case class Accounting
(
  productName: Name,
  trnCode: TrnCode,
  subjectNo: SubjectNo,
  brcType: AccountingBranchType,
  amtType: AccountingAmountType = Amount
  // 特征码: Option[String] = None
)

case class FoundAccounting(
                            productName: Name,
                            subjectNo: SubjectNo,
                            direction: AccountingDirection,
                            peers: List[Accounting]
                          )

final case class AccountingCommand(
                                    trn: TrnContext,
                                    commands: List[LedgerKey]
                                  )

final case class LedgerCommand(
                                brcNo: BranchNo,
                                subjectNo: SubjectNo,
                                direct: AccountingDirection,
                                amount: BiDec
                              )

case class LedgerKey(one: LedgerCommand, peers: List[LedgerCommand])

