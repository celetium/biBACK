package biback.domain.ledger

import biback.domain._
import biback.domain.account.BalanceChangeType
import biback.domain.common._
import biback.domain.common.model.{BranchLevel, Currency}
import biback.domain.product.SaleableProduct
import biback.utils.{GeneralAlgebra, MethodNoImplement}
import cats.Monad

trait LedgerAlgebra[F[_]] {
  val ledger: LedgerAlgebra.Service[F]
}

object LedgerAlgebra {

  trait Service[F[_]] extends GeneralAlgebra[F] {
    def getAccounting(productName: Name, trnCode: TrnCode, changeType: BalanceChangeType)(implicit F: Monad[F]): LogErrOrF[FoundAccounting] =
      errF(MethodNoImplement("getAccounting"))

    def getLedger(ccy: Currency, peers: List[(BranchNo, SubjectNo)])(implicit F: Monad[F]): LogErrOrF[List[Ledger]] =
      errF(MethodNoImplement("getLedger"))

    def getTransitSubjects()(implicit F: Monad[F]): LogErrOrF[Map[BranchLevel, SubjectNo]] =
      errF(MethodNoImplement("getTransitSubjects"))

    def getSubAcctInfo(acctId: AccountId, subAcctNo: SubAccountNo)(implicit F: Monad[F]): LogErrOrF[SubAcctInfo] =
      errF(MethodNoImplement("getSubAcctInfo"))
  }

}

case class SubAcctInfo(product: SaleableProduct, quotaBrcNo: BranchNo, openBrnNo: BranchNo)
