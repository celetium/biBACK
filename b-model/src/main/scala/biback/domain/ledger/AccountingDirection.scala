package biback.domain.ledger

import enumeratum._

sealed trait AccountingDirection extends EnumEntry {
  def peer: AccountingDirection
}

case object AccountingDirection extends Enum[AccountingDirection] with CirceEnum[AccountingDirection] {

  case object Debit extends AccountingDirection { def peer: AccountingDirection = Credit }
  case object Credit extends AccountingDirection  { def peer: AccountingDirection = Debit }

  val values = findValues
}

