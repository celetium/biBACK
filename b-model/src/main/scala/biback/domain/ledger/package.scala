package biback.domain

import biback.domain.account.BalanceChangeType
import biback.domain.common.model.Currency
import biback.domain.product.BaseProduct
import biback.utils.DomainError

package object ledger {
  case class AccountingNotFound(prd: BaseProduct, chgType: BalanceChangeType) extends DomainError

  case class LedgerNotFound(ccy: Currency, subjectNoList: List[(BranchNo, SubjectNo)]) extends DomainError

  case class NonAccountingForEvent(e: DomainEvent) extends DomainError

  case class DebitCreditNotEqual(one: LedgerCommand, peers: List[LedgerCommand]) extends DomainError

  case class SomeBranchNotFound(brcNoList: List[BranchNo]) extends DomainError
}
