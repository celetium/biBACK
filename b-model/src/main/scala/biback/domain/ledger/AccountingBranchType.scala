package biback.domain.ledger

import enumeratum._

sealed trait AccountingBranchType extends EnumEntry

case object AccountingBranchType extends Enum[AccountingBranchType] with CirceEnum[AccountingBranchType] {

  case object Open extends AccountingBranchType
  case object Quota extends AccountingBranchType
  case object Operation extends AccountingBranchType
  case object Superior extends AccountingBranchType
  case object Headquarter extends AccountingBranchType

  val values = findValues
}