package biback.domain.ledger

import enumeratum._

sealed trait AccountingAmountType extends EnumEntry

case object AccountingAmountType extends Enum[AccountingAmountType] with CirceEnum[AccountingAmountType] {

  case object Amount extends AccountingAmountType
  case object NetAmount extends AccountingAmountType
  case object Fee extends AccountingAmountType

  val values = findValues
}