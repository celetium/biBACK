package biback.domain.product

import biback.domain.ProductCode
import enumeratum._
import eu.timepit.refined.auto._

sealed abstract class BaseProduct(val code: ProductCode) extends EnumEntry with ProductPolicy

case object BaseProduct extends Enum[BaseProduct] with CirceEnum[BaseProduct] {

  case object CurrentDeposit1st extends BaseProduct("001") with CurrentDeposit1stProduct
  case object CurrentDeposit2nd extends BaseProduct("002") with CurrentDeposit2ndProduct
  // case object FixedDeposit extends BaseProduct("100") with FixedDepositProduct

  val values = findValues
}
