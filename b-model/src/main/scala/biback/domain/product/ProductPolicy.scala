package biback.domain.product

import biback.utils.DomainObject

trait ProductPolicy extends DomainObject {
  this: BaseProduct =>
  type OpenPreSetter
  def openPre(param: ProductParam, inputs: OpenPreSetter): ErrOrNone
  type OpenSetter
  def open(param: ProductParam, inputs: OpenSetter): ErrOrNone
  type DepositPreSetter
  def depositPre(param: ProductParam, inputs: DepositPreSetter): ErrOrNone
  type DepositSetter
  def deposit(param: ProductParam, inputs: DepositSetter): ErrOrNone
  type WithdrawPreSetter
  def withdrawPre(param: ProductParam, inputs: WithdrawPreSetter): ErrOrNone
  type WithdrawSetter
  def withdraw(param: ProductParam, inputs: WithdrawSetter): ErrOrNone
}
