package biback.domain.product

import biback.domain._
import biback.domain.common.model.{CashExchangeFlag, Currency}

case class SaleableProduct
(
  code: ProductCode,
  base: BaseProduct,
  ccy: Currency,
  cashTag: CashExchangeFlag,
  intNo: IntPriceNo,
  comment: Option[String] = None
)

case class ProductCurrency(product: BaseProduct, ccy: Currency, cashTag: CashExchangeFlag = CashExchangeFlag.Neither)

