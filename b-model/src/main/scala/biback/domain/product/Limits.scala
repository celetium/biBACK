package biback.domain.product

import biback.domain.TrnDate
import biback.domain.common.model.ChannelType
import enumeratum._

object Limits {
  sealed abstract class LimitType(val isMax: Boolean) extends EnumEntry

  case object LimitType extends Enum[LimitType] with CirceEnum[LimitType] {

    case object CurrentDeposit1stNum extends LimitType(true)
    case object DayInTrnAmtSum extends LimitType(true)
    case object DayOutTrnAmtSum extends LimitType(true)
    case object YearInTrnAmtSum extends LimitType(true)
    case object YearOutTrnAmtSum extends LimitType(true)

    val values = findValues
  }

  case class LimitKey(
                       limit: LimitType,
                       day: Option[TrnDate] = None,
                       month: Option[String] = None,
                       year: Option[String] = None,
                       channel: Option[ChannelType] = None
                     )

  object DayInTrnAmtSumMax {
    def apply(day: TrnDate): LimitKey =
      LimitKey(LimitType.DayInTrnAmtSum, Some(day))
  }

  object DayOutTrnAmtSumMax {
    def apply(day: TrnDate): LimitKey =
      LimitKey(LimitType.DayInTrnAmtSum, Some(day))
  }

  object QuantityMax {
    def apply(t: LimitType): LimitKey = LimitKey(t)
  }
}
