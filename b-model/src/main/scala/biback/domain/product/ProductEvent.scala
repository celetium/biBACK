package biback.domain.product

import enumeratum._

sealed trait ProductEvent extends EnumEntry

case object ProductEvent extends Enum[ProductEvent] with CirceEnum[ProductEvent] {
  val values = findValues

  case object Open extends ProductEvent
  case object Deposit extends ProductEvent
}
