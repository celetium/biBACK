package biback.domain

import biback.domain.common.model.{ChannelType, Currency}
import biback.domain.customer.{CustomerStatus, IdValidationType}
import biback.domain.product.Limits._
import biback.utils.DomainError

package object product {
  case class MinMaxLimitViolated(key: LimitKey, limit: NnDec, value: BiDec) extends DomainError

  case class MinMaxLimitNotFound(limit: LimitType) extends DomainError

  case class InvalidProductStatus(status: ProductStatus) extends DomainError

  case object NotCashingProduct extends DomainError

  case class ProductQuantityLimited(product: BaseProduct, quantity: Int) extends DomainError

  case class AccountQuantityLimited(product: BaseProduct, quantity: Int) extends DomainError

  case class InvalidCashingChannel(channel: ChannelType) extends DomainError

  case object EmptyInvoker extends DomainError

  case object EmptyCardBound extends DomainError

  case object EmptyPeerCardNo extends DomainError

  case object CertBoundRequired extends DomainError

  case class NotSatisfiedIdValidated(provided: IdValidationType, required: IdValidationType) extends DomainError

  case class InvalidCustomerStatus(status: CustomerStatus) extends DomainError

  case class AmtSumMaxLimited(product: BaseProduct, limit: LimitKey, max: BiDec) extends DomainError

  case class AmtSumMaxNotFound(product: BaseProduct, limit: LimitType) extends DomainError

  case class ProductNotFound(prdCode: ProductCode) extends DomainError

  case class ProductParamNotFound(product: BaseProduct) extends DomainError

  case class ProductPolicyNotFound(customerId: PoNum) extends DomainError

  case class CalcIntNotFound(from: TrnDate, to: TrnDate) extends DomainError

  case class InvalidOpenChannel(channel: ChannelType) extends DomainError

  case class InvalidOpenCurrency(ccy: Currency) extends DomainError

  case class InvalidOpenBranch(branch: BranchNo) extends DomainError

  case object ProductTrnInfoIsNull extends DomainError

  case object WebChannelRequireSMS extends DomainError

  case class ProductParamIsNull(product: BaseProduct) extends DomainError

  final case class SaleableProductNotFound(prdCode: ProductCode) extends DomainError

  final case class InterestNotFound(intNo: IntPriceNo) extends DomainError
}
