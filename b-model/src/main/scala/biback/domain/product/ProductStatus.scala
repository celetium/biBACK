package biback.domain.product

import enumeratum._

sealed trait ProductStatus extends EnumEntry

case object ProductStatus extends Enum[ProductStatus] with CirceEnum[ProductStatus] {

  case object Developing extends ProductStatus
  case object Opened extends ProductStatus
  case object Closed extends ProductStatus
  case object Upgrading extends ProductStatus
  case object Reopened extends ProductStatus

  val values = findValues
}