package spec.biback.domain

import biback.utils._
import biback.domain._
import biback.domain.common._
import biback.domain.customer._
import cats.Monad
import cats.data.EitherT
import org.scalatest.FlatSpec

trait RegisterSpec[F[_]] extends DomainSpec[F] {
  this: FlatSpec =>

  val trn1: TrnInitiation
  val mobile1: Mobile
  val mobile2: Mobile
  val sms1: SMSVCode
  val cNo1: CustomerNo
  lazy val created = PersonalCustomer(Personal(cNo1, mobile1, CustomerStatus.Raw))

  val customerIn: PersonalAlgebra.Service[F]
  val commonIn: CommonAlgebra.Service[F]
  lazy val algebras = new RegistryAlgebra[F] with PersonalAlgebra[F] with CommonAlgebra[F] {
    val registry = registryIn
    val customer = customerIn
    val common = commonIn
  }

  def register(implicit F: Monad[F]): Unit = {
    val register = new PersonalRegisterService(algebras)
    val runIt: RegisterRequest => EitherT[F, DomainError, Unit] = request =>
      bankSvc.contextOf(trn1).flatMap(register.run(_, request))
    it should "register OK" in {
      val request = RegisterRequest(mobile1, sms1)
      assert(runIt(request).value == Right(()))
      val saved = registryIn.read[PersonalEntity](cNo1.value)
      assert(saved.value == Right(created))
    }
    it should "register FAIL - MobileRegistered" in {
      val request = RegisterRequest(mobile1, sms1)
      assert(runIt(request).value == Left(MobileRegistered(mobile1)))
    }
    it should "register FAIL - SMSInvalid" in {
      val request = RegisterRequest(mobile2, sms1)
      assert(runIt(request).value == Left(SMSInvalid(mobile2, sms1)))
    }
  }
}
