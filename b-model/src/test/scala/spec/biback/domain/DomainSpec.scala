package spec.biback.domain

import biback.domain.common.{BankAlgebra, BankService, BranchAlgebra}
import biback.utils.RegistryAlgebra

trait DomainSpec[F[_]] {
  val registryIn: RegistryAlgebra.Service[F]
  lazy val registryAlgebra = new RegistryAlgebra[F] {
    val registry = registryIn
  }
  val branchIn: BranchAlgebra.Service[F]
  val bankIn: BankAlgebra.Service[F]
  lazy val bankAlgebra = new BankAlgebra[F] with BranchAlgebra[F] {
    val bank = bankIn
    val branch = branchIn
  }
  val bankSvc: BankService[F]
}
