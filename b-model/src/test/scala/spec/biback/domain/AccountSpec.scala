package spec.biback.domain

import biback.domain._
import biback.domain.account.BalanceType.AccountBalance
import biback.domain.account._
import biback.domain.cash.{BoxRequired, Initialized, TellerBoxEntity}
import biback.domain.common._
import biback.domain.common.model.ChannelType
import biback.domain.customer.{PersonalEntity, _}
import biback.domain.product.Limits.{LimitType, QuantityMax}
import biback.domain.product._
import biback.utils._
import cats.Monad
import cats.data.NonEmptyMap
import org.scalatest.FlatSpec
import eu.timepit.refined.auto._

trait AccountSpec[F[_]] extends DomainSpec[F] {
  this: FlatSpec =>

  val customerIn: PersonalAlgebra.Service[F]
  val productIn: ProductAlgebra.Service[F]
  val commonIn: CommonAlgebra.Service[F]
  lazy val algebras = new RegistryAlgebra[F] with PersonalAlgebra[F] with ProductAlgebra[F] with CommonAlgebra[F] {
    val registry = registryIn
    val customer = customerIn
    val product = productIn
    val common = commonIn
  }

  val trnDate: TrnDate
  val uuid1: TrnId
  val cert1: OneCertificate
  val teller1: TellerNo
  val idType1: PersonalIdType
  val idNo1: IdNo
  val cNo1: CustomerNo
  val mobile1: Mobile
  val product1: SaleableProduct
  val acctId1: AccountId
  val acctNo1: AccountNo
  val trnAmt1: NnDec
  val branch1: BranchNo

  lazy val es = Map[String, List[DomainEvent]](
    s"${classOf[PersonalEntity].getName}#$cNo1" -> List(
      Registered(cNo1, mobile1),
      IdValidated(idType1, idNo1)
    ),
    s"${classOf[TellerBoxEntity].getName}#$teller1" -> List(
      Initialized(teller1),
      BoxRequired()
    )
  )
  lazy val pre = PersonalCustomer(Personal(cNo1, mobile1, CustomerStatus.Normal))
  lazy val subNo: SubAccountNo = (product1.base.code.value + "00").check[SubAccountNoRule].toOption.get
  val balances = NonEmptyMap.of[BalanceType, BigDecimal](AccountBalance -> BigDecimal(0.00))
  lazy val acct1 = Account(acctId1, acctNo1, branch1, AccountStatus.Normal, NonEmptyMap.of((subNo, SubAccount(product1, balances, trnDate, branch1))))
  lazy val opened = pre.copy(accounts = Map(acct1.id -> acct1))
  lazy val added = balances.updateWith(AccountBalance)(_ + trnAmt1.value)
  lazy val acct1Added = acct1.copy(subs = acct1.subs.updateWith(subNo)(p => p.copy(balances = added)))
  lazy val deposited = opened.copy(accounts = Map(acct1.id -> acct1Added))

  def runIt[RQ, RS](trnCode: TrnCode, svc: DomainService[F, RQ, RS], request: RQ)(implicit F: Monad[F]) =
    bankSvc.contextOf(TrnInitiation(uuid1, ChannelType.Term, trnCode, Some(teller1))).flatMap(svc.run(_, request))

  def open(implicit F: Monad[F]): Unit = {
    val txOpen: TrnCode = "M001001"
    val open = new AccountOpenService(algebras)
    val request1 = OpenAccountRequest(idType1, idNo1, product1.code)
    val request2 = request1.copy(cert = Some(cert1))
    it should "open-account FAIL - CertBoundRequired" in {
      assert(runIt(txOpen, open, request1).value == Left(CertBoundRequired))
    }
    it should "open-account OK" in {
      assert(runIt(txOpen, open, request2).value == Right(()))
      val s = registryIn.read[PersonalEntity](cNo1.value)
      assert(s.value == Right(opened))
    }
    it should "open-account FAIL - AccountQuantityLimited" in {
      assert(runIt(txOpen, open, request2).value == Left(MinMaxLimitViolated(QuantityMax(LimitType.CurrentDeposit1stNum), NnDec1, 2)))
    }
  }

  def deposit(implicit F: Monad[F]): Unit = {
    val txDeposit: TrnCode = "F001001"
    val deposit = new AccountDepositService(algebras)
    it should "account-deposit OK" in {
      val request = CashDepositRequest(acctId1, subNo, trnAmt1)
      assert(runIt(txDeposit, deposit, request).value == Right(()))
      val s = registryIn.read[PersonalEntity](cNo1.value)
      assert(s.value == Right(deposited))
    }
  }

  def close(implicit F: Monad[F]): Unit = {
    val txClose: TrnCode = "M001002"
    val close = new AccountCloseService(algebras)
    it should "account-close OK" in {
      val request = CloseAccountRequest(acctId1, subNo)
      assert(runIt(txClose, close, request).value == Right(()))
    }
  }
}
