package spec.biback.domain

import biback.utils._
import biback.domain._
import biback.domain.common._
import biback.domain.customer._
import cats.Monad
import cats.data.EitherT
import org.scalatest.FlatSpec

trait IdentifySpec[F[_]] extends DomainSpec[F] {
  this: FlatSpec =>

  import CustomerStatus._

  val trn1: TrnInitiation
  val idType1: PersonalIdType
  val idNo1: IdNo
  val cNo1: CustomerNo

  def identify(implicit F: Monad[F]): Unit = {
    val identify = new PersonalIdentifyService[F](registryAlgebra)
    val runIt: IdValidateRequest => EitherT[F, DomainError, Unit] = request =>
      bankSvc.contextOf(trn1).flatMap(identify.run(_, request))
    it should "identify OK" in {
      val request = IdValidateRequest(cNo1, idType1, idNo1)
      assert(runIt(request).value == Right(()))
      val status = registryIn.read[PersonalEntity](cNo1.value).map(_.customer.status)
      assert(status.value == Right(Normal))
    }
    it should "identify FAIL - InvalidStateChange" in {
      val request = IdValidateRequest(cNo1, idType1, idNo1)
      assert(runIt(request).value == Left(InvalidStateChange(Normal.entryName, Normal.entryName)))
    }
  }
}