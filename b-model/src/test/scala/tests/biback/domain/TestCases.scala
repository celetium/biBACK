package tests.biback.domain

import biback.domain._
import biback.domain.account.{CertificateType, OneCertificate}
import biback.domain.common._
import biback.domain.common.model._, ChannelType._, Currency._, BranchLevel._
import biback.domain.customer._
import biback.domain.price._
import biback.domain.product._
import BaseProduct._
import Limits._
import eu.timepit.refined.auto._

trait TestCases {

  val trn1 = TrnInitiation("2088BE14-6F37-43D3-AB38-4F30E72B5E36", Web, "M000001")
  val mobile1: Mobile = "17712331001"
  val mobile2: Mobile = "13910407944"
  val sms1: SMSVCode = "123456"
  val cNo1: CustomerNo = "P0001"
  val idType1 = PersonalIdType.IdCard
  val idNo1: IdNo = "110108"

  // UUID created by OS command 'uuidgen'
  val uuid1: TrnId = "CF786F6F-57B7-403B-9BEE-C949E652F9D6"
  val uuid2: TrnId = "A4FDF115-314E-4CCF-90B9-D7B76C20F4BC"
  val uuid3: TrnId = "59F0E524-9B63-4BDC-B520-97F93A02EA3C"
  val cert1 = OneCertificate(CertificateType.DepositCard, "622884")
  val product1 = SaleableProduct("001", CurrentDeposit1st, CNY, CashExchangeFlag.Neither, "001001", Some("Testing"))
  val acctId1: AccountId = "A01001"
  val acctNo1: AccountNo = "A01001"
  val trnAmt1: NnDec = BigDecimal(3.00)
  val trnAmt2: NnDec = BigDecimal(10000.00)
  val feeAmt2: NnDec = BigDecimal(1.00)
  val teller1: TellerNo = "T01"
  val teller2: TellerNo = "T02"
  val branch0: BranchNo = "H00"
  val branch1: BranchNo = "B01"
  val branch2: BranchNo = "B02"
  val tellers = List[Teller](Teller(teller1, branch1), Teller(teller2, branch2))
  val branches = List[Branch](
    Branch(branch0, Headquarter),
    Branch(branch1, Branches),
    Branch(branch2, Branches)
  )

  val trnDate: TrnDate = "20190101"
  val localCurrency: Currency = CNY

  val channel2branch = Map[ChannelType, BranchNo](Web -> branch2)
  val product2params = Map[BaseProduct, ProductParam](
    CurrentDeposit1st -> ProductParam(
      openChannels = List(Term),
      cashing = true,
      cashingChannels = List(Term, ATM, Union),
      minMaxLimits = Map(
        LimitType.CurrentDeposit1stNum -> BigDecimal(1.00),
        LimitType.DayInTrnAmtSum -> BigDecimal(20000.00)
      ),
      idValidatedRequires = Map(ChannelType.Term -> IdValidationType.CounterValidated)
    ),
    CurrentDeposit2nd -> ProductParam(
      openChannels = List(Term),
      cashing = true,
      cashingChannels = List(Term, ATM, Union),
      minMaxLimits = Map(
        LimitType.CurrentDeposit1stNum -> BigDecimal(5.00),
        LimitType.DayInTrnAmtSum -> BigDecimal(20000.00)
      ),
      idValidatedRequires = Map(ChannelType.Web -> IdValidationType.FiveFactorValidated)
    )
  )
  val products = List[SaleableProduct](
    product1
  )
  val intKey1 = IntKey(IntCatalog.Deposit, CNY, TermType.Year, 0)
  val baseInts = List[BaseInt](
    BaseInt(intKey1, BigDecimal(3.60), "20190101"),
    BaseInt(intKey1, BigDecimal(5.40), "20190201")
  )
  val intPrices = List[IntPrice](
    IntPrice(product1.intNo, intKey1, 0, NnDec0, isActualDays = true, DayRateMultiples.X360, AdjustMethod.No)
  )

}
