package tests.biback.domain

object TestThinking {

  val headquarter = XBranch("0000")
  val headquarterFake = XBranch("9999")
  val superior1 = XBranch("0010", Some("0000"))
  val superior2 = XBranch("0021", Some("0000"))
  val branch11 = XBranch("A010", Some("0010"))
  val branch12 = XBranch("B010", Some("0010"))

  val s1 = TestCase("add superior", 1, Some("headquarter don't exists"))
  val sd1 = XAddUpBranchRequest(superior1.no)

  val h1 = TestCase("add headquarter", 0)
  val hd1 = XAddUpBranchRequest(headquarter.no)
  val h2 = TestCase("add headquarter", 1, Some("headquarter should exist only one"))
  val hd2 = hd1
  val h3 = TestCase("add headquarter", 1, Some("headquarter should exist only one"))
  val hd3 = XAddUpBranchRequest(headquarterFake.no)

  val b1 = TestCase("add branch", 1, Some("superior don't exists"))
  val bd1 = XAddBranchRequest(branch11.no, superior1.no)

  val s2 = TestCase("add superior", 0)
  val sd2 = XAddUpBranchRequest(superior1.no)
  val s3 = TestCase("add superior", 1, Some("superior has exists"))
  val sd3 = sd2

  val b2 = TestCase("add branch", 0)
  val bd2 = bd1
  val b3 = TestCase("add branch", 1, Some("branch has exists"))
  val bd3 = bd1
  val b4 = TestCase("add branch", 1, Some("up isn't superior"))
  val bd4 = XAddBranchRequest(branch12.no, branch11.no)
  val b5 = TestCase("add branch", 0)
  val bd5 = XAddBranchRequest(branch12.no, superior1.no)

  val flowHead = List(
    XAssert(1, "headquarter not found"),
    XDbInsert(2, "create headquarter")
  )
  val flowSuper = List(
    XAssert(1, "Branch not found"),
    XAssert(2, "headquarter found"),
    XDbInsert(3, "create super")
  )
  val flowBranch = List(
    XAssert(1, "Branch not found"),
    XAssert(2, "Up branch found"),
    XAssert(3, "Up branch should be super"),
    XDbInsert(4, "create branch")
  )
}

case class XBranch(no: String, up: Option[String] = None)
case class XAddUpBranchRequest(no: String)
case class XAddBranchRequest(no: String, up: String)

case class TestCase(name: String, failed: Int, tip: Option[String] = None)

case class XFlow()

case class XAssert(id: Int, name: String)
case class XDbInsert(id: Int, name: String)
