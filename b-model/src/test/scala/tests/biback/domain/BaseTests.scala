package tests.biback.domain

import java.math.{BigDecimal => BigDec}

import biback.domain.common.model.Currency.CNY
import biback.domain.common.Calculator
import biback.domain.common.model.CashExchangeFlag
import biback.domain.price._
import biback.domain.product.BaseProduct.CurrentDeposit1st
import biback.domain.product.SaleableProduct
import biback.domain.{NnDec0, TrnDate, _}
import cats.data.NonEmptyList
import eu.timepit.refined.auto._
import org.scalatest.FlatSpec

import scala.collection.immutable.SortedMap

class BaseTests extends FlatSpec {
  implicit val width: Int = 8
  behavior of "BigDecimal"
  it should "precision" in {
    println(BigDecimal(0))
    println(BigDecimal("0"))
    println(BigDecimal(0.00))
    println(BigDecimal("0.00"))
    val a = BigDecimal(1.01)
    val b = BigDecimal(1.02)
    val a1 = new BigDec(1.01)
    val b1 = new BigDec(1.02)
    val a1s = new BigDec("1.01")
    val b1s = new BigDec("1.02")
    println(a + b)
    println(a1.add(b1))
    println(a1s.add(b1s))
    println(a / BigDecimal(1))
    println(a % BigDecimal(1))
    println(a /% BigDecimal(1))
    val t1: TrnDate = "20190101"
    val t2: TrnDate = "20190102"
    assert((t2 - t1) == 1)
    val t11: TrnDate = "20190624"
    val t21: TrnDate = "20190721"
    assert((t21 - t11) == 27)
  }
  val product1 = SaleableProduct("001", CurrentDeposit1st, CNY, CashExchangeFlag.Neither, "001001", Some("Testing"))
  val intKey1 = IntKey(IntCatalog.Deposit, CNY, TermType.Year, 0)
  val intKey3 = IntKey(IntCatalog.Loan, CNY, TermType.Year, 0)
  val baseInt1 = BaseInt(intKey1, BigDecimal(36.0), "20190101")
  val baseInt2 = BaseInt(intKey1, BigDecimal(72.0), "20190105")
  val d1: TrnDate = "20190102"
  val d2: TrnDate = "20190103"
  val d3: TrnDate = "20190108"
  val accumulations = SortedMap(
    (d1, BigDecimal(100.00)),
    (d2, BigDecimal(200.00)),
    (d3, BigDecimal(500.00))
  )
  val price1 = IntPrice(product1.intNo, intKey1, 0, NnDec0, true, DayRateMultiples.X360, AdjustMethod.No)
  behavior of "Calculator"
  it should "calcInt - 无调息" in {
    val r = Calculator.calcInt(price1, NonEmptyList.of(baseInt1), accumulations, "20190101", "20190110")
    assert(r == BigDecimal(2.10))
  }
  it should "calcInt - 有调息有分段" in {
    val r = Calculator.calcInt(price1, NonEmptyList.of(baseInt1, baseInt2), accumulations, "20190101", "20190110")
    assert(r == BigDecimal(3.70))
  }
  it should "calcTermPay - 月供" in {
    val baseInt3 = BaseInt(intKey3, BigDecimal(5.25), "20190105")
    val price3 = IntPrice(product1.intNo, intKey3, 0, NnDec0, true, DayRateMultiples.X360, AdjustMethod.No)
    val r = Calculator.calcTermPay(12000.00, price3, baseInt3, 12)
    assert(r.roundX(CNY.minIntScale) == BigDecimal(1028.67))
  }
  it should "calcTermPay - 月供(消费易3)" in {
    val baseInt3 = BaseInt(intKey3, BigDecimal(4.75), "20190105")
    val price3 = IntPrice(product1.intNo, intKey3, 0, 10.00, true, DayRateMultiples.X360, AdjustMethod.No)
    def tryCMB(cap1: BiDec, loanDay: TrnDate, cmbInt: BiDec): Unit = {
      val p1 = Calculator.calcTermPayPlan(cap1, price3, baseInt3, 36, loanDay)
      println(s"Loan: $loanDay / ${cap1.setScale(CNY.minIntScale).format}, --------, ${cmbInt.format}")
      p1.foreach(_.print())
      println(p1.map(_.capAmt).sum.format(35))
    }
    tryCMB(76780.80, "20180424", 646.35)
    tryCMB(10550.00, "20181124", 88.81)
    tryCMB(72599.81, "20180124", 590.08)
    tryCMB(18202.30, "20190124", 147.94)
    tryCMB(4888.00, "20190224", 39.73)
    tryCMB(4516.00, "20190324", 38.02)
    tryCMB(5828.98, "20190524", 49.07)
    tryCMB(8818.00, "20190624", 74.23)
  }
}
