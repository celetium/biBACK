package tests.biback.domain

import cats.Id
import interpreter.biback.domain._
import spec.biback.domain._

class PersonalTest extends DomainMock[Id]
  with RegisterSpec[Id]
  with IdentifySpec[Id]
  with RegisterSpecMock[Id]
  with BranchMock[Id]
  with TestCases {
  "Personal/Customer/Register" should behave like register
  "Personal/Customer/Identify" should behave like identify
}
