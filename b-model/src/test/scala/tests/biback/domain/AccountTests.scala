package tests.biback.domain

import biback.domain.common.CommonAlgebra
import cats.Id
import interpreter.biback.domain._
import spec.biback.domain.AccountSpec

class AccountTests extends DomainMock[Id]
  with AccountSpec[Id]
  with AccountSpecMock[Id]
  with BranchMock[Id]
  with ProductMock[Id]
  with TestCases {

  override val registryIn = new RegistryMock[Id](es)
  val commonIn = CommonAlgebra.noImpl[Id]

  "Personal/Account/Open" should behave like open
  "Personal/Account/Deposit" should behave like deposit
  "Personal/Account/Close" should behave like close
}
