package tests.biback.domain

import biback.domain._
import biback.domain.account.BalanceType.AccountBalance
import biback.domain.account._
import biback.domain.common._
import biback.domain.common.model._
import biback.domain.customer.BalanceChanged
import biback.domain.ledger._
import AccountingBranchType._
import biback.domain.common.model.BranchLevel
import biback.utils._
import cats.Id
import cats.data.NonEmptyMap
import com.typesafe.scalalogging.LazyLogging
import eu.timepit.refined.auto._
import interpreter.biback.domain._
import org.scalatest.FlatSpec

class LedgerTests extends FlatSpec
  with LedgerMock[Id]
  with BranchMock[Id]
  with TestCases
  with LazyLogging {

  import AccountingAmountType._
  import AccountingDirection._
  import BalanceChangeType._

  val productFamilies = Map[Name, Option[Name]](
    ("TellerBox", None),
    ("Deposit", None),
    ("CurrentDeposit", Some("Deposit")),
    ("CurrentDeposit1st", Some("CurrentDeposit"))
  )
  val productAccountingRules = List[ProductAccounting](
    ProductAccounting("CurrentDeposit", "231001", Credit)
  )
  val peerAccountingRules = List(
    Accounting("CurrentDeposit", "F001001", "1010", Operation), // 存现
    Accounting("CurrentDeposit", "F002001", "311006", Headquarter, NetAmount), // 银联转账转出
    Accounting("CurrentDeposit", "F002001", "52300117", Headquarter, Fee) // 银联转账转出
  )
  val transitRules = Map[BranchLevel, SubjectNo](
    BranchLevel.Branches -> "312002",
    BranchLevel.Superiors -> "312001",
    BranchLevel.Headquarter -> "312003"
  )
  val ledgers = List[Ledger](
    Ledger(1L, branch1, product1.ccy, "231001", 1, NnDec0, AccountStatus.Normal),
    Ledger(2L, branch1, product1.ccy, "1010", 1, NnDec0, AccountStatus.Normal),
    Ledger(3L, branch0, product1.ccy, "311006", 1, NnDec0, AccountStatus.Normal),
    Ledger(4L, branch0, product1.ccy, "52300117", 1, NnDec0, AccountStatus.Normal),
    Ledger(5L, branch2, product1.ccy, "1010", 1, NnDec0, AccountStatus.Normal),
    Ledger(6L, branch2, product1.ccy, "312001", 1, NnDec0, AccountStatus.Normal),
    Ledger(7L, branch2, product1.ccy, "312002", 1, NnDec0, AccountStatus.Normal),
    Ledger(8L, branch2, product1.ccy, "312003", 1, NnDec0, AccountStatus.Normal)
  )
  val subNo: SubAccountNo = (product1.base.code.value + "00").check[SubAccountNoRule].toOption.get
  val balances = NonEmptyMap.of[BalanceType, BigDecimal](AccountBalance -> BiDec0)
  val accounts = List[Account](
    Account(acctId1, acctNo1, branch1, AccountStatus.Normal, NonEmptyMap.of((subNo, SubAccount(product1, balances, trnDate, branch1))))
  )
  val ctx1 = TrnContext(uuid1, ChannelType.Term, "F001001", trnDate, TrnDuty(branch1, Some(teller1)), TrnDuty(branch1, Some(teller1)))
  val event1 = BalanceChanged(ctx1, acctId1, subNo, balances, `+`, trnAmt1)
  val cmd1 = AccountingCommand(ctx1, List(
    LedgerKey(
      LedgerCommand(branch1, "231001", Credit, trnAmt1),
      List(LedgerCommand(branch1, "1010", Debit, trnAmt1)))))
  val ctx2 = TrnContext(uuid2, ChannelType.Term, "F002001", trnDate, TrnDuty(branch1, Some(teller1)), TrnDuty(branch1, Some(teller1)))
  val event2 = BalanceChanged(ctx2, acctId1, subNo, balances, `-`, trnAmt2.value + feeAmt2.value, feeAmt2)
  val cmd2 = AccountingCommand(ctx2, List(
    LedgerKey(
      LedgerCommand(branch1, "231001", Debit, event2.amount),
      List(LedgerCommand(branch1, transitRules(BranchLevel.Headquarter), Credit, event2.amount))),
    LedgerKey(
      LedgerCommand(branch0, transitRules(BranchLevel.Headquarter), Debit, event2.amount),
      List(
        LedgerCommand(branch0, "311006", Credit, trnAmt2),
        LedgerCommand(branch0, "52300117", Credit, feeAmt2)))
  ))
  val ctx3 = TrnContext(uuid3, ChannelType.Term, "F001001", trnDate, TrnDuty(branch2, Some(teller2)), TrnDuty(branch2, Some(teller2)))
  val event3 = BalanceChanged(ctx3, acctId1, subNo, balances, `+`, trnAmt1)
  val cmd3 = AccountingCommand(ctx3, List(
    LedgerKey(
      LedgerCommand(branch1, "231001", Credit, trnAmt1),
      List(LedgerCommand(branch1, transitRules(BranchLevel.Branches), Debit, trnAmt1))),
    LedgerKey(
      LedgerCommand(branch2, transitRules(BranchLevel.Branches), Credit, trnAmt1),
      List(LedgerCommand(branch2, "1010", Debit, trnAmt1)))
  ))

  val service = new LedgerService[Id](algebras)
  behavior of "Accounting"
  it should "本机构存现" in {
    val r = service.onEvent(event1)
    r.value.logTo(logger)
    if (r.value.isLeft) fail("Failed")
    else assert(r.value.right.get == cmd1)
  }
  it should "银联转账（带手续费）" in {
    val r = service.onEvent(event2)
    r.value.logTo(logger)
    if (r.value.isLeft) fail("Failed")
    else assert(r.value.right.get == cmd2)
  }
  it should "跨机构存现" in {
    val r = service.onEvent(event3)
    r.value.logTo(logger)
    if (r.value.isLeft) fail("Failed")
    else assert(r.value.right.get == cmd3)
  }
}
