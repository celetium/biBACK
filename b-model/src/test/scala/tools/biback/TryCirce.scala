package tools.biback

import biback.domain.BranchNo
import biback.domain.common.model.{Branch, BranchLevel}
import io.circe._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._
import io.circe.refined._
import eu.timepit.refined.auto._

case class Branch1(no: BranchNo, level: BranchLevel, b: Float)
case class Branch2(no: BranchNo, level: BranchLevel)
object TryCirce extends App {
  val b = Branch("999", BranchLevel.Headquarter)
  val json = b.asJson.noSpaces
  println(json)
  val bX = decode[Branch1](json)
  println(bX)
  val bY = decode[Branch2](json)
  val a: DecodingFailure = null
  println(bY)
  val bZ = decode[Branch](json)
  println(bZ)
}
