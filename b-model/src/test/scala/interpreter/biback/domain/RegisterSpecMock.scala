package interpreter.biback.domain

import biback.domain.common.{CommonAlgebra, SMSInvalid}
import biback.domain.customer._
import biback.domain.{CustomerNo, Mobile, SMSVCode}
import biback.utils.RegistryAlgebra
import cats.Monad

trait RegisterSpecMock[F[_]] {
  val registryIn: RegistryAlgebra.Service[F]
  val mobile1: Mobile
  val sms1: SMSVCode
  val cNo1: CustomerNo
  val customerIn = new PersonalAlgebra.Service[F] {
    override def createNo(catalog: CustomerCatalog)(implicit F: Monad[F]): ErrOrF[CustomerNo] =
      resultF(cNo1)

    override def getNoByMobile(mobile: Mobile)(implicit F: Monad[F]): ErrOrF[Option[CustomerNo]] = {
      for {
        p <- registryIn.refFor[PersonalEntity](cNo1.value)
        c <- resultF(p.state.filter(_.customer.mobile == mobile).map(_.customer.no))
      } yield c
    }
  }
  val commonIn = new CommonAlgebra.Service[F] {
    override def smsValidated(mobile: Mobile, sms: SMSVCode)(implicit F: Monad[F]): ErrOrNoneF =
      assertF(mobile == mobile1 && sms == sms1, SMSInvalid(mobile, sms))
  }

}
