package interpreter.biback.domain

import biback.domain._
import biback.domain.common._
import biback.domain.common.model._
import cats.Monad

trait BranchMock[F[_]] {
  val tellers: List[Teller]
  val branches: List[Branch]
  val channel2branch: Map[ChannelType, BranchNo]
  val branchIn = new BranchAlgebra.Service[F] {
    override def getTeller(no: TellerNo)(implicit F: Monad[F]): ErrOrF[Option[Teller]] =
      resultF(tellers.find(_.no == no))

    override def getBranchNo(channel: ChannelType)(implicit F: Monad[F]): ErrOrF[Option[BranchNo]] =
      resultF(channel2branch.get(channel))

    override def foundHeadBranch()(implicit F: Monad[F]): ErrOrF[Branch] =
      foundO(branches.find(_.level == BranchLevel.Headquarter), HeadBranchNotFound)

    override def getBranches(nos: Set[BranchNo])(implicit F: Monad[F]): ErrOrF[List[Branch]] = {
      val found = branches.filter(p => nos.contains(p.no))
      orF(found.length == nos.size, found, SomeBranchesNotFound(nos.diff(found.map(_.no).toSet)))
    }
  }
}
