package interpreter.biback.domain

import biback.domain._
import biback.domain.common._
import biback.domain.common.model.Currency
import cats.Monad

trait BankMock[F[_]] {
  val trnDate: TrnDate
  val localCurrency: Currency
  val bankIn = new BankAlgebra.Service[F] {
    override def getTrnDate(implicit F: Monad[F]): ErrOrF[TrnDate] =
      resultF(trnDate)

    override def isLocalCcy(ccy: Currency)(implicit F: Monad[F]): ErrOrF[Boolean] =
      resultF(ccy == localCurrency)
  }
}
