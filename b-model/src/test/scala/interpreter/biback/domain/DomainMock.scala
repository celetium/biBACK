package interpreter.biback.domain

import biback.domain.common.BankService
import cats.Monad
import org.scalatest.FlatSpec
import spec.biback.domain.DomainSpec

abstract class DomainMock[F[_] : Monad] extends FlatSpec with DomainSpec[F] with BankMock[F] {
  val registryIn = new RegistryMock[F]
  lazy val bankSvc = new BankService(bankAlgebra)
}
