package interpreter.biback.domain

import biback.utils._
import biback.domain._
import biback.domain.account.Account
import biback.domain.customer._
import cats.Monad
import scala.collection.immutable.SortedMap

trait AccountSpecMock[F[_]] {
  val registryIn: RegistryAlgebra.Service[F]
  val idType1: PersonalIdType
  val idNo1: IdNo
  val cNo1: CustomerNo
  val acctId1: AccountId
  val acctNo1: AccountNo
  val acct1: Account
  val customerIn = new PersonalAlgebra.Service[F] {
    override def foundCustomerProduct(acctId: AccountId, subAcctNo: SubAccountNo)(implicit F: Monad[F]): ErrOrF[CustomerProduct] =
      for {
        p <- registryIn.refFor[PersonalEntity](cNo1.value)
        c <- p.found
        x <- orF(c.foundSubAccount(acctId, subAcctNo).map(s => CustomerProduct(c.customer.no, s.product)))
      } yield x

    override def foundCustomerBase(idType: PersonalIdType, idNo: IdNo)(implicit F: Monad[F]): ErrOrF[CustomerBase] =
      for {
        _ <- assertF(idType == idType1 && idNo == idNo1, CustomerIdNotFound(idType, idNo))
        p <- registryIn.refFor[PersonalEntity](cNo1.value)
        c <- p.found
      } yield CustomerBase(c.customer.no, c.customer.mobile)

    override def createAcctId()(implicit F: Monad[F]): ErrOrF[AccountId] =
      resultF(acctId1)

    override def createAcctNo()(implicit F: Monad[F]): ErrOrF[AccountNo] =
      resultF(acctNo1)

    override def getSubAcctBalanceChanges(acctId: AccountId, subAcctNo: SubAccountNo, to: TrnDate)(implicit F: Monad[F]): ErrOrF[SubAcctBalanceChanges] =
      resultF(SubAcctBalanceChanges(acct1.subs.head._2.calcIntFrom, SortedMap.empty))
  }

}
